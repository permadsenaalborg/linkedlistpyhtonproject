import unittest
from LinkedList import LinkedList
from LinkedList import Element

class TestElement(unittest.TestCase):

    def setUp(self):
        print("Running test ...")

    def tearDown(self):
        print("Done!")

    def test_constructor_elemString(self):
        el = Element('Per')
        self.assertIsNotNone(el)

    def test_constructor_elemInteger(self):
        el = Element(37)
        self.assertIsNotNone(el)

    def test_compare(self):
        el1 = Element(37)
        el2 = Element(465)
        self.assertFalse(el1.isGreater(el2))
        self.assertTrue(el2.isGreater(el1))

        el1 = Element('Abe')
        el2 = Element('Hund')
        self.assertFalse(el1.isGreater(el2))
        self.assertTrue(el2.isGreater(el1))

        el1 = Element(37)
        el2 = Element('Hund')
        self.assertFalse(el1.isGreater(el2))
        self.assertTrue(el2.isGreater(el1))


class TestLinkedList(unittest.TestCase):

    def setUp(self):
        print("Running test ...")

    def tearDown(self):
        print("Done!")

    def test_constructor_list(self):
        ll = LinkedList()
        self.assertIsNotNone(ll)

    def test_addToEmpty(self):
        ll = LinkedList()
        self.assertEqual(ll.count(), 0)
        ll.addFirst(37)
        self.assertIsNotNone(ll)
        self.assertEqual(ll.count(), 1)

    def test_addToNonEmpty(self):
        ll = LinkedList()
        self.assertEqual(ll.count(), 0)
        ll.addFirst(37)
        ll.addFirst(65)
        self.assertIsNotNone(ll)
        self.assertEqual(ll.count(), 2)

    def test_addMany(self):
        ll = LinkedList()
        for t in range(1000):
            ll.addFirst(t)

    def test_removeFromEmpty(self):
        ll = LinkedList()
        ll.removeFirst()
        self.assertIsNotNone(ll)

    def test_removeNonEmpty(self):
        ll = LinkedList()
        ll.addFirst(37)
        ll.removeFirst()
        self.assertIsNotNone(ll)

    def test_basis(self):
        ll = LinkedList()

        ll.addFirst(86)
        ll.addFirst(90)
        ll.addFirst(34)
        ll.addFirst(67)
        ll.addFirst(46)
        self.assertEqual(ll.count(), 5)

    def test_sort(self):
        val1 = 67
        val2 = 334
        val3 = 23

        ll = LinkedList()
        ll.addFirst(val1)
        ll.addFirst(val2)
        ll.addFirst(val3)
        self.assertEqual(ll.count(), 3)
        self.assertEqual(ll.getFirst(), val3)

        ll.sort()
        self.assertEqual(ll.count(), 3)
        self.assertEqual(ll.getFirst(), val3)
        ll.removeFirst()
        self.assertEqual(ll.getFirst(), val1)
        ll.removeFirst()
        self.assertEqual(ll.getFirst(), val2)
        ll.removeFirst()
        self.assertIsNone(ll.getFirst())

    def test_sortEmpty(self):
        ll = LinkedList()
        ll.sort()
        self.assertIsNotNone(ll)

    def test_remove_one(self):
        ll = LinkedList()
        self.assertEqual(ll.count(), 0)
        ll.addFirst("Per")
        self.assertEqual(ll.count(), 1)
        self.assertEqual(ll.getFirst(), "Per")
        ll.removeFirst()
        self.assertEqual(ll.count(), 0)
        self.assertTrue(ll.getFirst() is None)

    def test_remove_three(self):
        ll = LinkedList()
        self.assertEqual(ll.count(), 0)
        ll.addFirst(34)
        ll.addFirst(67)
        ll.addFirst(456)
        self.assertEqual(ll.count(), 3)
        self.assertEqual(ll.getFirst(), 456)
        ll.removeFirst()
        self.assertEqual(ll.count(), 2)
        self.assertEqual(ll.getFirst(), 67)
        ll.removeFirst()
        self.assertEqual(ll.count(), 1)
        self.assertEqual(ll.getFirst(), 34)
        ll.removeFirst()
        self.assertEqual(ll.count(), 0)
        self.assertTrue(ll.getFirst() is None)
        ll.removeFirst()
        self.assertTrue(ll.count() == 0)
        self.assertTrue(ll.getFirst() is None)


if __name__ == '__main__':
    unittest.main()
