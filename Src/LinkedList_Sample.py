from LinkedList import LinkedList

ll = LinkedList()

ll.addFirst(34)
ll.addFirst(12)

print(ll)
ll.print()
print()

ll.addFirst(56)
ll.addFirst(43)

print(ll)
ll.print()
print()

for t in range(4):
    print("t: " + str(t))
    ll.removeFirst()
    print(ll)
    ll.print()
    print()
